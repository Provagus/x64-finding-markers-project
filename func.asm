;I will refer to color as 0 for black and 1 for anything else
;So red and green are both 1 and are same color in this code

section	.text
global find_markers

find_markers:
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    ; rdi bmp array pointer
    ; rsi x array pointer
    ; rdx y array pointer (will be stored on stack)
    ; Extra reserving space for local variables
    ; rbp-8 height
    ; rbp-16 width
    ; rbp-24 line size
    ; rbp -32 offset
    ; rbp -40 start of pixel array
    ; rbp -48 y array pointer
    sub rsp, 48 ;reserving space for extra variables
    
    push rbx

    mov rbx, rdi ;pixel array pointer
    mov [rbp-48], rdx ;storing y array pointer

    push rbx ;arg1 = bmp
    call is_bmp
    pop rbx ;pop argument from stack
    mov rcx, rax
    mov rax, -1
    cmp rcx, 0 ;check if is bmp
    jne quit_function ;if not quit
    
    push rbx ;pushing argument
    call get_height ;checking height
    pop rbx
    mov rcx, rax
    mov [rbp-8], rax ;storing result on stack
    mov rax, -1
    cmp rcx, 0 ;check if height is positive
    jle quit_function ;if not quit

    push rbx ;pushing argument
    call get_width ;checking width
    pop rbx
    mov rcx, rax
    mov [rbp-16], rax ;storing result on stack
    mov rax, -1
    cmp rcx, 0 ;check if width is positive
    jle quit_function ;if not quit

    mov rbx, rdi
    push rbx ;pushing argument
    call get_offset ;checking offset
    pop rbx
    mov [rbp-32], rax ;storing result on stack

    mov rcx, rdi ;load start of bmp
    add rcx, [rbp-32] ;bmp start + offset = first pixel
    mov [rbp-40], rcx ;save first pixel location

    mov rbx, [rbp-16] ;loading width argument
    push rbx ;pushing argument to stack
    call get_line_size ;getting line width
    pop rbx
    mov [rbp-24], rax ;storing result

    push QWORD [rbp-48] ;load y array pointer
    push QWORD rsi ;load x array pointer
    push QWORD [rbp-24] ;load line size argument
    push QWORD [rbp-40] ;load first pixel argument
    push QWORD [rbp-16] ;load max width argument
    push QWORD [rbp-8] ;load max height argument
    call start_main_loop_y
    add rsp, 48 ;pop arguments
    ;in rax should be result

quit_function:
    pop rbx
    add rsp, 48 ;removing extra variables

    pop rbp
    ret

is_bmp:
    ;Arguments:
    ; +16 bmp array
    ;Returns 1 if is not bmp file and 0 if is
    push rbp
    mov rbp, rsp
    push rbx
    push rcx

    mov rax, 1
    mov rbx, [rbp+16]
    mov cl, [rbx] ;loading first byte of header
    cmp cl, 0x42 ;checking if is 42
    jne quit_is_bmp
    mov cl, [rbx+1] ;loading second byte of header
    cmp cl, 0x4D ;checking if is 4D
    jne quit_is_bmp
    mov rax, 0

quit_is_bmp:
    pop rcx
    pop rbx
    pop rbp
    ret

get_height:
    ;Arguments:
    ; +16 bmp array
    ;Returning height in rax
    push rbp
    mov rbp, rsp ;rbp holds now base pointer

    mov rdx, [rbp+16] ;set start of array to rax
    xor rax, rax ;zeroing register
    mov eax, [rdx+22] ;rax = height

    pop rbp
    ret

get_width:
    ;Arguments:
    ; +16 bmp array
    ;Returning width in rax
    push rbp
    mov rbp, rsp ;rbp holds now base pointer

    mov rdx, [rbp+16] ;set start of array to rax
    xor rax, rax ;zeroing register
    mov eax, [rdx+18] ;rax = width

    pop rbp
    ret

get_offset:
    ;Arguments:
    ; +16 bmp array
    ;Returning offset in rax
    push rbp
    mov rbp, rsp ;rbp holds now base pointer

    mov rdx, [rbp+16] ;set start of array to rax
    xor rax, rax ;zeroing register
    mov eax, [rdx+10] ;rax = offset

    pop rbp
    ret

get_line_size:
    ;Arguments:
    ; +16 width
    ;Returning line size in rax
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    push rbx

    mov rcx, [rbp+16] ;moving width to rcx
    shl rcx, 1
    add rcx, [rbp+16]
    mov rax, rcx ;moving width to rax for division
    mov rdx, 0
    mov rbx, 4
    div rbx ;div to get rest from division
    ;so we can check if we need to increase line size to next number divisible by 4
    mov rcx, rdx ;rdx will change, so moving result
    mul rbx ;in rax we have div result, so by multiplication by 4 we will get
    ;size of line divisible by 4 equal or smaller (by less than 4) than real line size
    cmp rcx, 0 ;check if mod == 0
    je quit_get_line_size
    add rax, 4 ;if mod % 4 != 0 we add 4

quit_get_line_size:
    pop rbx
    pop rbp
    ret

get_pixel:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x
    ; +56 y
    ;Returning 0 if black 1 if not black or out of bmp file
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    
    mov rax, 1 ;placing non black color
    mov rcx, [rbp+48] ;rcx = x
    cmp rcx, [rbp+24] ;x >= width
    jge quit_get_pixel

    cmp rcx, 0
    jl quit_get_pixel ;x < 0

    mov rcx, [rbp+56] ;rcx = y
    cmp rcx, [rbp+16] ;y >= height
    jge quit_get_pixel

    cmp rcx, 0 ;y < 0
    jl quit_get_pixel
    
    mov rcx, [rbp+16] ;rcx = max height
    mov rdx, [rbp+56] ;rdx = y
    sub rcx, rdx ;y = max height - y
    dec rcx ;rcx--
    ;since top is at bottom

    mov rax, [rbp+40] ;rax = size of line
    mul QWORD rcx ;size of line * y
    
    mov rcx, [rbp+48] ;rcx = x
    shl rcx, 1
    add rcx, [rbp+48] ;rcx = 3x
    add rax, rcx ;size of line *y + 3x

    add rax, [rbp+32] ;adress of pixel
    mov rcx, 0 ;zeroing register
    mov cl, [rax] ;loading first color
    inc rax
    mov ch, [rax] ;loading second color
    inc rax
    shl rcx, 16 ;moving one color to the left
    mov cl, [rax] ;loading third color

    mov rax, 0
    cmp rcx, 0
    je quit_get_pixel ;checking if is black
    mov rax, 1

quit_get_pixel:
    pop rbp
    ret

start_main_loop_y:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x array pointer
    ; +56 y array pointer
    ;Returning nothing
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    ;reserving extra space
    ; -8 markers found
    sub rsp, 8
    push rbx
    
    mov rbx, 0 ;i = 0
    mov QWORD [rbp-8], 0 ;markers found = 0

main_loop_y:
    mov rax, [rbp-8]
    cmp rbx, [rbp+16]
    jge end_main_loop_y ;i < max height to continue

    mov rcx, rbp
    sub rcx, 8 ;load pointer position to rbp 
    push rcx ;load pointer to markers found
    push QWORD [rbp+56] ;load y array
    push QWORD [rbp+48] ;load x array
    push rbx ;load y argument
    push QWORD [rbp+40] ;size of line argument
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;argument max width
    push QWORD [rbp+16] ;load max height as argument
    call start_main_loop_x
    add rsp, 64 ;pop arguments

    inc rbx ;i++
    jmp main_loop_y

end_main_loop_y:
    pop rbx
    add rsp, 8
    pop rbp
    ret

start_main_loop_x:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 y
    ; +56 x array pointer
    ; +64 y array pointer
    ; +72 pointer to markers found
    ;Returning nothing
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    push rbx
    mov rbx, 0 ;j = 0

main_loop_x:
    cmp rbx, [rbp+24]
    jge end_main_loop_x ;j < max width to continue

    ;checking current pixel color
    push QWORD [rbp+48] ;y
    push rbx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    cmp rax, 0
    jne next_main_loop_x ;if pixel is not black then check next pixel
    
    push QWORD [rbp+72] ;pointer to markers found
    push QWORD [rbp+64] ;y array
    push QWORD [rbp+56] ;x array
    push QWORD [rbp+48] ;y
    push rbx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call check_pixel
    add rsp, 72 ;pop arguments

next_main_loop_x:
    inc rbx ;j++
    jmp main_loop_x

end_main_loop_x:
    pop rbx
    pop rbp
    ret

check_pixel:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x
    ; +56 y
    ; +64 x array pointer
    ; +72 y array pointer
    ; +80 pointer to markers found
    ;appending pixel to array if it's marker
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    ;Reserving extra space for variables
    sub rsp, 24
    ; -8 arms length
    ; -16 diagonal pixel x
    ; -24 diagonal pixel y
    push rbx

    mov rcx, [rbp+48] ;rcx = x
    inc rcx ;x++

    push QWORD [rbp+56] ;y
    push rcx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    cmp rax, 0
    je quit_check_pixel ;pixel is black not valid marker

    ;checking pixel below
    mov rcx, [rbp+56] ;rcx = y
    inc rcx ;y++

    push rcx ;y
    push QWORD [rbp+48] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    cmp rax, 0
    je quit_check_pixel ;pixel is black not valid marker

    ;left and bottom pixel not black, potentially marker conrner
    push QWORD [rbp+56] ;y
    push QWORD [rbp+48] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call start_arms
    add rsp, 48 ;pop arguments

    cmp rax, 0
    jle quit_check_pixel ;not marker, leave

    mov [rbp-8], rax ;saving arms length

    ;now looping through diagonal pixels as long as they are black
    mov rbx, 1 ;i = 1

check_pixel_internal_loop:
    cmp rbx, [rbp-8]
    je quit_check_pixel ;if i==arm length exit

    ; Checking diagonal pixel
    mov rcx, [rbp+56] ;loading y
    sub rcx, rbx ;y-currently checked pixel
    mov [rbp-24], rcx ;storing for future
    push rcx ;y

    mov rcx, [rbp+48] ;loading x
    sub rcx, rbx ;x-currently checked pixel
    mov [rbp-16], rcx ;storing for future
    push rcx ;x

    ;we pushed diagonal pixel location now
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    cmp rax, 0
    jne after_checking_black_pixels ;checking if pixel is black, if no continue

    ;sub marker arm length
    mov rcx, [rbp-8] ;loading length to rcx
    sub rcx, rbx ;length - i
    inc rcx ;+1 to check white field at the end
    push rcx ;length to check
    push QWORD [rbp-24] ;diagonal y
    push QWORD [rbp-16] ;diagonal x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call verify_black_arm
    add rsp, 56 ;pop arguments
    cmp rax, 0
    je quit_check_pixel ;arms not correct, quit pixel checking

    inc rbx ;i++
    jmp check_pixel_internal_loop
after_checking_black_pixels:
    ;checking internal margin
    ;rbx should be increased from previous stage
    mov rcx, [rbp-8] ;loading length to rcx
    sub rcx, rbx ;length - i
    cmp rcx, 0
    je quit_check_pixel ;it is black square, quit pixel check

    push rcx ;length to check
    push QWORD [rbp-24] ;y
    push QWORD [rbp-16] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call verify_margin ;veryfing internal margin
    add rsp, 56 ;pop arguments
    cmp rax, 0
    je quit_check_pixel ;if not correct quit

    ;checking extrenal margin now
    mov rcx, [rbp+56] ;loading y
    inc rcx ;y++ pixel below
    mov rdx, [rbp+48] ;loading x
    inc rdx ;x++ pixel on right
    push QWORD [rbp-8] ;length to check
    push rcx ;y
    push rdx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call verify_margin ;veryfing internal margin
    add rsp, 56 ;pop arguments
    cmp rax, 0
    je quit_check_pixel ;if not correct quit

    ;now we confirmed that it is correct marker
    mov rcx, [rbp+80] ;load pointer
    mov rbx, [rcx] ;load value of found markers
    mov rdx, [rbp+64] ;load x array
    mov rax, [rbp+48]
    mov [rdx+4*rbx], rax ;save x to array
    mov rdx, [rbp+72] ;load y array
    mov rax, [rbp+56]
    mov [rdx+4*rbx], rax ;save y to array
    inc QWORD [rcx] ;increase number of markers found

quit_check_pixel:
    pop rbx
    add rsp, 24
    pop rbp
    ret

start_arms:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x
    ; +56 y
    ;appending pixel to array if it's marker
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    sub rsp, 8 ;space for extra variables
    ; -8 tmp variable

    push rbx
    mov rbx, 1

arms:
    mov rcx, [rbp+48] ;rcx = x
    sub rcx, rbx ;x - i

    push QWORD [rbp+56] ;y
    push rcx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    mov [rbp-8], rax

    mov rcx, [rbp+56] ;rcx = y
    sub rcx, rbx ;y - i

    push rcx ;y
    push QWORD [rbp+48] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments

    mov rdx, rax ; rdx = color
    mov rax, -1 ;negative value in case if not valid marker
    cmp rdx, [rbp-8]
    jne quit_arms ;quit if colors are different

    mov rax, rbx ;load proper value to return
    cmp rdx, 0
    jne quit_arms ;exit if color is not black

    inc rbx ;i++
    jmp arms ;continue loop

quit_arms:
    pop rbx
    add rsp, 8 ;clear space for extra variables
    pop rbp
    ret

verify_black_arm:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x
    ; +56 y
    ; +64 length of arm to check
    ;returns 0 if not correct, 1 if correct
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    ;Reserving extra space
    ; -8 color of pixel above
    sub rsp, 8
    push rbx
    mov rbx, 0 ;i = 0

loop_verify_black_arm:
    mov rax, 1 ;if last loop then it is result
    cmp rbx, [rbp+64]
    jge quit_verify_black_arm ;i < arm length

    mov rcx, [rbp+56] ;rcx = y
    sub rcx, rbx ;checking pixel above
    push rcx ;y
    push QWORD [rbp+48] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    mov [rbp-8], rax ;storing result

    mov rcx, [rbp+48] ;rcx = y
    sub rcx, rbx ;checking pixel on the left
    push QWORD [rbp+56] ;y
    push rcx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments

    mov rdx, rax
    mov rax, 0 ;loading 0 in case if not correct
    cmp rdx, [rbp-8] ;checking if pixel above and on the left have same color
    jne quit_verify_black_arm ;if not leave

    mov rcx, 1 ;load not black
    inc rbx ;i++
    cmp rbx, [rbp+64]
    je skip_change_color
    mov rcx, 0 ;load black
skip_change_color:
    mov rax, 0 ;load 0 in case of fail
    cmp rdx, rcx
    jne quit_verify_black_arm ;quit if no match

    ;continue checking
    jmp loop_verify_black_arm

quit_verify_black_arm:
    pop rbx
    add rsp, 8
    pop rbp
    ret

verify_margin:
    ;Arguments:
    ; +16 max height
    ; +24 max width
    ; +32 start of pixel array
    ; +40 size of line
    ; +48 x
    ; +56 y
    ; +64 length of arm to check
    ;returns 0 if not correct, 1 if correct
    push rbp
    mov rbp, rsp ;rbp holds now base pointer
    ;Reserving extra space
    ; -8 color of pixel above
    sub rsp, 8
    push rbx
    mov rbx, 0 ;i = 0

loop_verify_margin:
    mov rax, 1 ;if last loop then it is result
    cmp rbx, [rbp+64]
    jge quit_verify_margin ;i < arm length

    mov rcx, [rbp+56] ;rcx = y
    sub rcx, rbx ;checking pixel above
    push rcx ;y
    push QWORD [rbp+48] ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments
    mov [rbp-8], rax ;storing result

    mov rcx, [rbp+48] ;rcx = y
    sub rcx, rbx ;checking pixel on the left
    push QWORD [rbp+56] ;y
    push rcx ;x
    push QWORD [rbp+40] ;size of line
    push QWORD [rbp+32] ;start of pixel array
    push QWORD [rbp+24] ;max width
    push QWORD [rbp+16] ;max height
    call get_pixel
    add rsp, 48 ;pop arguments

    mov rdx, rax
    mov rax, 0 ;loading 0 in case if not correct
    cmp rdx, [rbp-8] ;checking if pixel above and on the left have same color
    jne quit_verify_margin ;if not leave

    mov rax, 0 ;load 0 in case of fail
    cmp rdx, 0
    je quit_verify_margin ;quit if black

    ;continue checking
    inc rbx ;i++
    jmp loop_verify_margin

quit_verify_margin:
    pop rbx
    add rsp, 8
    pop rbp
    ret