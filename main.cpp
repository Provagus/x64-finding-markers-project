#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

extern "C" int find_markers (
    unsigned char *bitmap, 
    unsigned int *x_pos, 
    unsigned int *y_pos
);

using namespace std;

int result;
string filename;
string filecontent;
unsigned int xArray[100];
unsigned int yArray[100];

int main(int argc, char *argv[]){
    if (argc == 1){
        cout<<"Filename parameter missing\n";
        return 0;
    }
    filename = argv[1];
    ifstream file;
    file.open(filename.c_str());
    if(file.is_open()){
        stringstream input;
        while(file.peek() != EOF){
            input << (char) file.get();
        }
        filecontent = input.str();
        file.close();
    }
    else{
        cout << "Program couldn't open file\n";
        return 0;
    }
    unsigned char * bmp = (unsigned char*)filecontent.c_str(); 
    /*Since it's content of bmp file and content will be calculated by assembly function
    it doesn't actually matter if we consider values as signed or not*/
    result = find_markers(bmp, xArray, yArray);
    if(result < 0){
        cout<<"Program enountered error. Please make sure given file is correct."<<endl;
        return 0;
    }
    for(int i = 0; i < result; i++){
        cout<<*(xArray+i)<<", "<<*(yArray+i)<<endl;
    }
    return 0;
}