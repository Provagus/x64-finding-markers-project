CC=g++
ASMBIN=nasm

all : asm cc link
asm : 
	$(ASMBIN) -o func.o -f elf64 -g -l func.lst func.asm
cc :
	$(CC) -c -g -O0 main.cpp &> errors.txt
link : main.o func.o
	$(CC) -g -o find_markers main.o func.o
clean :
	rm *.o
	rm find_markers
	rm errors.txt	
	rm func.lst